### Java Spring boot application

For this application you have to have installed MySQL database from https://dev.mysql.com/downloads/mysql/.
Installed MySQL has to have database named **mysqldatabase** running on port **3306**.

To run the springboot application copy the repository to your local SimpleMessageServer folder with git https://git-scm.com/downloads.

Go to SimpleMessageServer folder and run the spring boot backend server on linux terminal:

**.\mvnw spring-boot:run**

on windows cmd:

**mvnw.cmd spring-boot:run**

To run the frontend server go to app folder and run:

**yarn add bootstrap react-cookie react-router-dom reactstrap**

**yarn && yarn start**