package com.example.application.controler;

import com.example.application.model.AuthorMessage;
import com.example.application.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class MessageController {

    private static Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private MessageRepository messageRepository;

    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping("/messages")
    Collection<AuthorMessage> messages() {
        logger.info("GET Request for all mesages.");
        return messageRepository.findAll();
    }

    @GetMapping("/messages/{author}")
    Collection<AuthorMessage> getMessages(@PathVariable String author) {
        logger.info("GET Request for author mesages for author:" + author);
        return messageRepository.findByAuthor(author);
    }

    @GetMapping("/message/{id}")
    Optional<AuthorMessage> getMessage(@PathVariable Long id) {
        logger.info("GET Request to get message:" + id);
        return messageRepository.findById(id);
    }

    @PostMapping(produces = "application/json", value = "/message")
    ResponseEntity<AuthorMessage> createMessage(@Valid @RequestBody AuthorMessage message) {
        logger.info("PUT Request to create message");
        AuthorMessage result = messageRepository.save(message);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping(produces = "application/json", value = "/message")
    ResponseEntity<AuthorMessage> updateMessage(@Valid @RequestBody AuthorMessage message) {
        logger.info("PUT Request to update message:" + message.getId());
                AuthorMessage result = messageRepository.save(message);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping(produces = "application/json", value = "/message/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long id) {
        logger.info("DELETE Request to delete message:" + id);
        messageRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
