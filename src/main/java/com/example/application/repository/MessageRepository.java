package com.example.application.repository;

import com.example.application.model.AuthorMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Transactional
public interface MessageRepository extends JpaRepository<AuthorMessage, Long> {
    Collection<AuthorMessage> findByAuthor(String author);
}
