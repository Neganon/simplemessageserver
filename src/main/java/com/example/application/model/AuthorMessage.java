package com.example.application.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "message_table")
public class AuthorMessage {

    @Id
    @GeneratedValue
    private long id;
    @NonNull
    private String author;
    @NonNull
    private String messageText;
    private ZonedDateTime messageTime;
}
