import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { instanceOf } from 'prop-types';
import { Cookies, withCookies } from 'react-cookie';

class AuthorMessageEdit extends Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  emptyItem = {
    author: '',
    text: ''
  };

  constructor(props) {
    super(props);
    const {cookies} = props;
    this.state = {
      item: this.emptyItem,
      username: cookies.get('username')
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    if (this.state.username === undefined){
      this.props.history.push('/');
    }
  }

  async componentDidMount() {
    if (this.props.match.params.id !== 'new') {
      try {
        const message = await (await fetch(`/message/${this.props.match.params.id}`)).json();
        this.setState({item: message});
      } catch (error) {
        this.props.history.push('/');
      }
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = {...this.state.item};
    item[name] = value;
    this.setState({item});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {item} = this.state;
    if (item.author === '') {
      item.author = this.state.username;
    }

    await fetch('/message', {
      method: (item.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
      credentials: 'include'
    });
    this.props.history.push('/messages');
  }

  render() {
    const {item} = this.state;
    const title = <h2>{item.id ? 'Edit Message' : 'Add Message'}</h2>;

    return <div>
      <AppNavbar/>
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="author">Author Name</Label>
            <Input readOnly type="text" name="author" id="author" value={item.author || this.state.username}
                   onChange={this.handleChange} autoComplete="author"/>
          </FormGroup>
          <FormGroup>
            <Label for="message">Text</Label>
            <Input type="text" name="messageText" id="messageText" value={item.messageText || ''}
                   onChange={this.handleChange} autoComplete="messageText"/>
          </FormGroup>
          <FormGroup>
            <Button color="primary" type="submit">Save</Button>{' '}
            <Button color="secondary" tag={Link} to="/messages">Cancel</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}
export default withCookies(withRouter(AuthorMessageEdit));
