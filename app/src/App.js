import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AuthorMessageList from './AuthorMessageList';
import AuthorMessageEdit from './AuthorMessageEdit';
import Login from "./Login";
import { CookiesProvider } from 'react-cookie';

class App extends Component {
    render() {
        return (
            <CookiesProvider>
                <Router>
                    <Switch>
                        <Route path='/' exact={true} component={Login}/>
                        <Route path='/login' exact={true} component={Login}/>
                        <Route path='/messages' exact={true} component={AuthorMessageList}/>
                        <Route path='/message/:id' component={AuthorMessageEdit}/>
                    </Switch>
                </Router>
            </CookiesProvider>
        )
    }
}

export default App;