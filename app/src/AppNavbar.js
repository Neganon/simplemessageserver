import React, { Component } from 'react';
import { Nav, Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import { Cookies, withCookies } from 'react-cookie';
import { instanceOf } from 'prop-types';

class AppNavbar extends Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    const {cookies} = props;
    this.state = {username: cookies.get('username')
    };
  }

  render() {
    return <Navbar color="dark" dark expand="md">
      <NavbarBrand tag={Link} to="/">Relogin</NavbarBrand>
        <Nav className="ml-auto" navbar>
          <NavItem>
            <NavLink>{this.state.username}</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="https://gitlab.com/Neganon/simplemessageserver">GitLab</NavLink>
          </NavItem>
        </Nav>
    </Navbar>;
  }
}
export default withCookies(withRouter(AppNavbar));