import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link, withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';
import ReactLoading from 'react-loading';
import Select from "react-dropdown-select";

class AuthorMessageList extends Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    const {cookies} = props;
    this.state = {messages: [],
      filteredMessages: [],
      authors: ['Author'],
      isLoading: true,
      username: cookies.get('username'),
      filterAuthor: ''
    };
    this.remove = this.remove.bind(this);
    if (this.state.username === undefined){
      this.props.history.push('/');
    }
  }

  componentDidMount() {
    this.setState({isLoading: true});

    if(this.state.filterAuthor === undefined || this.state.filterAuthor === '') {
      fetch('/messages')
          .then(response => response.json())
          .then(data => this.setState({messages: data, isLoading: false}))
          .catch(() => this.props.history.push('/'));
    }
    else {
      fetch(`/messages/${this.state.filterAuthor}`)
          .then(response => response.json())
          .then(data => this.setState({filteredMessages: data, isLoading: false}))
          .catch(() => this.props.history.push('/'));
    }
  }

  async remove(id) {
    await fetch(`/message/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(() => {
      let updatedMessages = [...this.state.messages].filter(i => i.id !== id);
      this.setState({messages: updatedMessages});
    });
  }

  handleOnChange = (event) => {
    if(event.length > 0){
      this.state.filterAuthor = event[0].value;
      this.componentDidMount();
    }
    else  {
      this.state.filterAuthor = '';
      this.componentDidMount();
    }
  }
  render() {
    const {messages, filteredMessages, isLoading} = this.state;

    if (isLoading) {
      return <div align="center">
            <ReactLoading type="bubbles" color="black" height={600} width={350} />
            </div>;
    }

    const messageList = this.state.filterAuthor === '' ? messages.map(message => {
      return <tr key={message.id}>
        <td>{message.author}</td>
        <td>{message.messageText || ''}</td>
        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/message/" + message.id}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(message.id)}>Delete</Button>
          </ButtonGroup>
        </td>
      </tr>
    })
        :
        filteredMessages.map(message => {
          return <tr key={message.id}>
            <td>{message.author}</td>
            <td>{message.messageText || ''}</td>
            <td>
              <ButtonGroup>
                <Button size="sm" color="primary" tag={Link} to={"/message/" + message.id}>Edit</Button>
                <Button size="sm" color="danger" onClick={() => this.remove(message.id)}>Delete</Button>
              </ButtonGroup>
            </td>
          </tr>
        })
    ;

    const authors = Array.from(new Set(messages.map(message => message.author)));

    const items = authors.map((author, i) =>{
      return ( {label:author, value: author}
      )}, this);

    const val = '';

    return (
      <div>
        <AppNavbar/>
        <Container fluid>
          <div className="float-left">
            <Button color="success" tag={Link} to="/message/new">Add Message</Button>
          </div>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="20%">
                <Select options={items} multiple={false} clearable="true" searchable={true} value={val}
                        onChange={this.handleOnChange}></Select>
              </th>
              <th width="20%">Message</th>
            </tr>
            </thead>
            <tbody>
            {messageList}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}
export default withCookies(withRouter(AuthorMessageList));