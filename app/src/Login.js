import React, { Component } from "react";
import { Button, FormGroup, FormControl, FormLabel  } from "react-bootstrap";
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import "./Login.css";
import { withCookies, Cookies } from 'react-cookie';

class Login extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            name: "",
        };
    }

    validateForm() {
        return this.state.name.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();

        try {
            const cookies = new Cookies();
            cookies.set('username', this.state.name, { path: '/' });
            this.props.history.push('/messages');
        } catch (e) {
            alert(e.message);
        }
    }

    render() {
        return (
            <div className="Login">
                <h3 align="center">Simple messaging</h3>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="name">
                        <FormLabel >Name</FormLabel>
                        <FormControl autoFocus type="name" value={this.state.name} onChange={this.handleChange}/>
                    </FormGroup>
                    <Button block disabled={!this.validateForm()} type="submit">
                        Login
                    </Button>
                </form>
            </div>
        );
    }
}
export default withCookies(withRouter(Login));